<?PHP
    session_start();
    require_once('connect.php');
    if($_SESSION['mail'] == NULL){
        header('location: helloPage.php');
    }
    if(isset($_GET['logout'])){
        $_SESSION['mail'] == NULL;
        unset($_SESSION['mail']);
        header('location: helloPage.php');
    }

    $connect = mysqli_connect($host, $db_user, $db_password) or die ('Serwer jest wyłączony');
    mysqli_select_db($connect, $db_name) or die ('Baza danych jest wyłączona');

    $connect_first_login = mysqli_query($connect, 'SELECT pierwszyLogin FROM konta WHERE mail = "'.$_SESSION['mail_db'].'";');
    while($connect_row = mysqli_fetch_array($connect_first_login)){
        $firstLoginSession = $connect_row['pierwszyLogin'];
    }
    $_SESSION['pierwszyLogin'] = $firstLoginSession;
    if($_SESSION['pierwszyLogin']==NULL){
        header("Location: firstLoginPage.php");
    }
    else{
        $connect_post_max_id = mysqli_query($connect, 'SELECT ID FROM posty;');
        while($connect_row = mysqli_fetch_array($connect_post_max_id)){
            $max_ID = max($connect_row['ID'],$connect_row['ID']);
        }
        $rand_ID = rand(1,$max_ID);
        }
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>artCollection - inspiruj siebie i innych</title>
    <link rel="Shortcut icon" href="css/img/logo.png" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fontello.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  
    <script src="js/functions.js"></script>
</head>
<body>
    <div class="navHelloPage">
        <div class="title">artCollection</div>&nbsp;
        <div class="userInfo">
            <?PHP
                if(isset($_SESSION['mail']))
                {
                    echo $_SESSION['mail'].'</br>';
                }
            ?>
            <a href="postPage.php?logout=true">Wyloguj się</a>
        </div>
    </div>
    <div class="postContainer">
        <div class="post">
            <?PHP
                echo '<img src="css/img/img'.$rand_ID.'.jpg"></img>'
            ?>
            <div class="postInfoLeft">
                <?PHP
                    $connect_post_max_id = mysqli_query($connect, 'SELECT autor,typ,kraj FROM posty WHERE ID = '.$rand_ID.';');
                    while($connect_row = mysqli_fetch_array($connect_post_max_id)){
                        echo "<h5>".$connect_row['autor']."</h5>";
                        echo "<h6>".$connect_row['typ']."</h6>";
                        echo "<h6>".$connect_row['kraj']."</h6>";
                    }
                ?>
            </div>
            <div class="postInfoRight">
                <h5></h5>
            </div>
        </div></br>
        <a href="postPage.php">Nowy post</a>
    </div>
</body>
</html>