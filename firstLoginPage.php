<?PHP
    session_start();
    /*if($_SESSION['login']==NULL){
        header('location: helloPage.php');
    }*/
    require_once("connect.php");

    $check = true;
    if(empty($_POST['wiek'])){
        $_SESSION['error_age_empty'] = "Wiek nie może pozostać pusty!";
        $check = false;
    }
    if(empty($_POST['szkola'])){
        $_SESSION['error_school_empty'] = "Szkoła nie może pozostać pusta, np.: Zespół Szkół Fotograficznych w Olsztynie";
        $check = false;
    }
    if(empty($_POST['praca'])){
        $_SESSION['error_job_empty'] = "Stanowisko nie może pozostać puste, np.: Database developer lub Kasjer";
        $check = false;
    }
    if($check == true){

       $connect = mysqli_connect($host, $db_user, $db_password) or die ('Serwer jest wyłączony');
        mysqli_select_db($connect, $db_name) or die ('Baza danych jest wyłączona');

        mysqli_query($connect,'UPDATE `konta` SET wiek = "'.$_POST['wiek'].'", pierwszyLogin = "false", facebook = "'.$_POST['facebook'].'", google = "'.$_POST['google'].'", szkola = "'.$_POST['szkola'].'", praca = "'.$_POST['praca'].'" WHERE mail = "'.$_SESSION['mail'].'";');
        echo "dziala";
        header('Location: postPage.php');
    }
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>artCollection - inspiruj siebie i innych</title>
    <link rel="Shortcut icon" href="css/img/logo.png" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fontello.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  
    <script src="js/functions.js"></script>
</head>
<body>
    <div class="navHelloPage">
        <div class="title">artCollection</div>&nbsp;
        <div class="userInfo">
            <?PHP
                if(isset($_SESSION['mail']))
                {
                    echo $_SESSION['mail'].'</br>';
                }
            ?>
            <a href="postPage.php?logout=true">Wyloguj się</a>
        </div>
    </div>
    <div class="firstLoginContainer">
        <div class="firstLoginHeader">
            <h1>Hej, jesteś tu pierwszy raz?</h1>
            <h1>Poznaj artCollection</h1></br>
            <h2>artCollection to strona,która ma za zadanie połączyć <b>Zdjęcia</b>, <b>Rysunki</b>, <b>Filmy</b> i inne dzieła sztuki w jedno, by zlikwidować brak inspiracji.</br>
            <h2>Uzupełnij proszę te dane, aby prościej było się z Tobą komunikować!</h2></br><i class="demo-icon icon-down-dir-1"></i>
        </div>
        <div class="firstLoginInput">
        <form method="POST">
            <input type="text" name="wiek" placeholder="Wpisz wiek"></input>
            <div class="firstLoginInfo">
                <b>Dlaczego mam uzupełniać wiek?</b></br>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            </div>
            <?PHP
                if(isset($_SESSION['error_age_empty']))
                {
                    echo '<div class="error">'.$_SESSION['error_age_empty'].'</div>';
                    unset($_SESSION['error_age_empty']);
                }
            ?>
            <div style="clear: both"></div>
            <input type="text" name="facebook" placeholder="Podaj facebooka"></input>
            <div class="firstLoginInfo">
                <b>Masz facebooka?</b></br>Podaj link do profilu facebooka, aby łatwiej było się z Tobą skomunikować!
                </br><i>*konto facebook nie jest wymagane!</i>
            </div>
            <div style="clear: both"></div>

            <input type="text" name="google" placeholder="Podaj google+"></input>
            <div class="firstLoginInfo">
                <b>Masz google+?</b></br>Podaj link do profilu google+, aby łatwiej było się z Tobą skomunikować!
                </br><i>*konto google+ nie jest wymagane!</i>
            </div>
            <div style="clear: both"></div>

            <input type="text" name="szkola" placeholder="Podaj szkołę"></input>
            <div class="firstLoginInfo">
                <b>Jaką masz skończoną szkołe?</b></br>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            </div>
            <?PHP
                if(isset($_SESSION['error_school_empty']))
                {
                    echo '<div class="error">'.$_SESSION['error_school_empty'].'</div>';
                    unset($_SESSION['error_school_empty']);
                }
            ?>
            <div style="clear: both"></div>

            <input type="text" name="praca" placeholder="Podaj miejsce pracy"></input>
            <div class="firstLoginInfo">
                <b>Czym się zajmujesz?</b></br>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            </div>
            <?PHP
                if(isset($_SESSION['error_job_empty']))
                {
                    echo '<div class="error">'.$_SESSION['error_job_empty'].'</div>';
                    unset($_SESSION['error_job_empty']);
                }
            ?>
            <input type="submit" value="Zakończ!"></input>
        </form>
        <div style="clear: both"></div>
        </div>
    </div>
</body>
</html>