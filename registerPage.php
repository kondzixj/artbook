<?PHP
    session_start();
    $check = true;

    $login = $_POST['login'];
    $haslo = $_POST['haslo'];
    $haslo2nd = $_POST['haslo2nd'];
    $mail = $_POST['mail'];
    $surname = $_POST['surname'];
    $town = $_POST['town'];
    //login
    if(empty($login)){
        $check = false;
        $_SESSION['error_login_empty'] = "Imie jest wymagane!";
    }
    //mail
    if(empty($mail)){
        $check = false;
        $_SESSION['error_mail_empty'] = "Mail jest pusty!";
    }
    if(!filter_var($mail, FILTER_VALIDATE_EMAIL)){
        $check = false;
        $_SESSION['error_mail_text'] = "Mail jest błędny!";
    }
    //haslo
    if((strlen($haslo)<10)){
        $check = false;
        $_SESSION['error_password_length'] = "Haslo musi się składać z conajmniej 10 znaków!";
    }
    if(!preg_match('@[0-9]+@', $haslo)){
        $check = false;
        $_SESSION['error_password_numbers'] = "Haslo musi posiadać chociaż jedną cyfrę!";
    }
    //drugie haslo
    if($haslo != $haslo2nd){
        $check = false;
        $_SESSION['error_2ndpassword_fit'] = "Hasła muszą się zgadzać!";
    }
    if(empty($surname)){
        $check = false;
        $_SESSION['error_surname_empty'] = "Nazwisko nie może pozostać puste!";
    }
    if(empty($town)){
        $check = false;
        $_SESSION['error_town_empty'] = "Miasto nie może pozostać puste!";
    }
    //baza
    require_once('connect.php');
    if($check == true){
        $connect = mysqli_connect($host, $db_user, $db_password) or die ('Serwer jest wyłączony');
        mysqli_select_db($connect, $db_name) or die ('Baza danych jest wyłączona');

        mysqli_query($connect,'INSERT INTO `konta` (`id`, `imie`, `nazwisko`, `mail`, `miasto`, `haslo`) VALUES (NULL, "'.$login.'","'.$surname.'","'.$mail.'","'.$town.'","'.password_hash($haslo, PASSWORD_DEFAULT).'");');
        
        mysqli_close($connect);
        header('Location : helloPage.php');
        echo '<style>body{margin: 0px; padding:0px;}</style><div style="font-family: arial;font-size: 32px;text-align: center;width: 100%;height: 95.9vh;padding: 20px 0px 20px 0px;background-color: #17b55a;color: #d0f0de;text-align: center;">Konto zostalo założone</br> <a style="color: #d0f0de;font-weight: bold;" href="helloPage.php">Zaloguj się </a></div>';
        exit();
    }
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>artBook - inspiruj siebie i innych</title>
    <link rel="Shortcut icon" href="css/img/logo.png" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fontello.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  
    <script src="js/functions.js"></script>
</head>
<body>
    <div class="navHelloPage">
        <div class="title">artBook</div>&nbsp;
    </div>
    <div class="containerHelloPage">
        <div class="tileInput">
            <h2>Co to jest artBook?</h2>
            <h3>artBook jest serwisem społecznościowym, polegającym na
                dzieleniu się inspiracją w postaci zdjęć rysunków lub filmów
                z innymi.</h3>
        </div>
    </div>
    <div class="containerHelloPage">
        <div class="tileInput">
            <h2>Dołącz do nas!</h2>
            <h3>Zaloguj się,</br>a jeżeli nie masz konta - zarejestruj.</h3>
            <form method="post">
                <input name="login" type="text" placeholder="Podaj imie" />
                <?PHP
                    if(isset($_SESSION['error_login_empty']))
                    {
                        echo '<div class="error">'.$_SESSION['error_login_empty'].'</div>';
                        unset($_SESSION['error_login_empty']);
                    }
                ?>
                <input name="surname" type="text" placeholder="Podaj nazwisko" />
                <?PHP
                    if(isset($_SESSION['error_surname_empty']))
                    {
                        echo '<div class="error">'.$_SESSION['error_surname_empty'].'</div>';
                        unset($_SESSION['error_surname_empty']);
                    }
                ?>
                <input name="town" type="text" placeholder="Podaj Miasto" />
                <?PHP
                    if(isset($_SESSION['error_town_empty']))
                    {
                        echo '<div class="error">'.$_SESSION['error_town_empty'].'</div>';
                        unset($_SESSION['error_town_empty']);
                    }
                ?>
                <input name="mail" type="text" placeholder="Podaj maila" />
                <?PHP
                    if(isset($_SESSION['error_mail_empty']))
                    {
                        echo '<div class="error">'.$_SESSION['error_mail_empty'].'</div>';
                        unset($_SESSION['error_mail_empty']);
                    }
                    if(isset($_SESSION['error_mail_text']))
                    {
                        echo '<div class="error">'.$_SESSION['error_mail_text'].'</div>';
                        unset($_SESSION['error_mail_text']);
                    }
                ?>
                <input name="haslo" type="password" placeholder="Podaj hasło" />
                <?PHP
                    if(isset($_SESSION['error_password_length']))
                    {
                        echo '<div class="error">'.$_SESSION['error_password_length'].'</div>';
                        unset($_SESSION['error_password_length']);
                    }
                    if(isset($_SESSION['error_password_numbers']))
                    {
                        echo '<div class="error">'.$_SESSION['error_password_numbers'].'</div>';
                        unset($_SESSION['error_password_numbers']);
                    }
                ?>
                <input name="haslo2nd" type="password" placeholder="Powtórz hasło" />
                <?PHP
                    if(isset($_SESSION['error_2ndpassword_fit']))
                    {
                        echo '<div class="error">'.$_SESSION['error_2ndpassword_fit'].'</div>';
                        unset($_SESSION['error_2ndpassword_fit']);
                    }
                ?>
                <input style="width: 30%; background-color: #17b55a; color: #d0f0de;" type="submit" value="Zarejestruj" />
                <?PHP
                    if(isset($_SESSION['success_register']))
                    {
                        echo '<div class="success">'.$_SESSION['success_register'].'</div>';
                        unset($_SESSION['success_register']);
                    }
                ?>
            </form>
        </div>
    </div>
    <p class="demo-icon icon-down-dir-1"></p>
    <div class="footerHelloPage">
        <div class="containerFooterHelloPage">
            artBook &copy;
            <hr>
            <a href="#">cos tam</a><a href="#">cos tam</a><a href="#">cos tam</a><a href="#">cos tam</a>
        </div>
    </div>
</body>
</html>