<?PHP
    session_start();
    require_once('connect.php');

    if($_SESSION['mail'] == NULL ){
        header('location: helloPage.php');
    }
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>artCollection - inspiruj siebie i innych</title>
    <link rel="Shortcut icon" href="css/img/logo.png" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fontello.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  
    <script src="js/functions.js"></script>
</head>
<body>
    <div class="navHelloPage">
        <div class="title">artCollection</div>&nbsp;
        <div class="userInfo">
            <?PHP
                if(isset($_SESSION['mail']))
                {
                    echo $_SESSION['mail'].'</br>';
                }
            ?>
            <a href="postPage.php?logout=true">Wyloguj się</a>
        </div>
    </div>
    <div class="profileContainer">
        <div class="profileContainerNav">
            <ul>
                <li>
                    <?PHP
                        if(isset($_SESSION['mail']))
                        {
                            echo $_SESSION['mail'].' 18,';
                            echo $_SESSION['town'];
                        }
                    ?>
                </li>
                <li id="facebook">
                    <i class="demo-icon icon-facebook"></i> 
                </li>
                <li id="google">
                    <i class="demo-icon icon-googleplus"></i>
                </li>
                <li id="proffesion">
                    fotograf
                </li>
            </ul>
        </div>
        <div style="clear:both"></div>
        <div class="profileContent">
            <div class="profileBox">
                <div class="profileBoxTitle">
                    Informacje
                </div>
                <div class="profileBoxText">
                    <ul>
                        <li>Szkoła</li>
                        <li>Praca</li>
                    </ul>
                </div>
            </div>
            <div class="profileBox">
                <div class="profileBoxTitle">
                    Zdjęcia
                </div>
                <div class="profileBoxText">
                    <div class="profileBoxImgContainer">
                        <div class="profileBoxImg">
                        </div>
                        <div class="profileBoxImg">
                        </div>
                        <div class="profileBoxImg">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>