<?PHP
    session_start();

    require_once('helloPage.php'); 
    require_once('connect.php'); 
    require_once('postPage.php'); 

    $mail = $_POST['mail'];
    $haslo = $_POST['haslo'];

    $_SESSION['mail'] = $mail;
    $_SESSION['haslo'] = $haslo;

    if($mail == NULL || $haslo == NULL)
    {
        header('Location: helloPage.php');
        $_SESSION['error_login_blank'] = "Login i hasło nie mogą pozostać puste!";
        exit();
    }
    $connect = mysqli_connect($host, $db_user, $db_password) or die ('Serwer jest wyłączony');
    mysqli_select_db($connect, $db_name) or die ('Baza danych jest wyłączona');

    $mail_db = mysqli_real_escape_string($connect, $mail);
    $_SESSION['mail_db'] = $mail_db;
    $haslo_db = mysqli_real_escape_string($connect, $haslo);

    $connect_result_mail = mysqli_query($connect, 'SELECT mail FROM konta WHERE mail = "'.$mail_db.'";');
    $connect_row_mail = mysqli_fetch_array($connect_result_mail);
    $connect_string_mail = $connect_row_mail['mail'];

    $connect_result_password = mysqli_query($connect, 'SELECT haslo FROM konta WHERE mail = "'.$mail_db.'";');
    $connect_row_password = mysqli_fetch_array($connect_result_password);
    $connect_string_password = $connect_row_password['haslo'];
    
    if(password_verify($haslo, $connect_string_password) && $mail == $connect_string_mail){
        header('Location: postPage.php');
    }  else {
        header('Location: helloPage.php');
        $_SESSION['error_login_and_password'] = "Nieprawidłowy login lub hasło!";
        exit();
    } 
    
    



    //dodatkowe dane
    $connect_profile_town = mysqli_query($connect, 'SELECT miasto FROM konta WHERE mail = "'.$mail_db.'";');
    while($connect_row = mysqli_fetch_array($connect_profile_town)){
        $townSession = $connect_row['miasto'];
    }
    $_SESSION['town'] = $townSession;
?>