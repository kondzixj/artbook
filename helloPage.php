<?PHP
    session_start();
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>artCollection - inspiruj siebie i innych</title>
    <link rel="Shortcut icon" href="css/img/logo.png" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fontello.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  
    <script src="js/functions.js"></script>
</head>
<body>
    <div class="navHelloPage">
        artCollection
    </div>
    <div class="containerHelloPage">
        <div class="tileImage">
            <img src="css/img/img1.jpg">
        </div>
        <div class="tileImageTwo">
            <h1>Szukaj inspiracji, </br>lub pomóż znaleźć ją innym.</h1>
            <img src="css/img/img2.jpg"></img>
        </div>
    </div>
    <div class="containerHelloPage">    
        <div class="tileInput">
            <h2>Dołącz do nas!</h2>
            <h3>Zaloguj się,</br>a jeżeli nie masz konta - zarejestruj.</h3>
            <form action="loginEngine.php" method="post">
                <input name="mail" type="text" placeholder="Podaj mail" />
                <?PHP
                    if(isset($_SESSION['error_login_blank']))
                    {
                        echo '<div class="error">'.$_SESSION['error_login_blank'].'</div>';
                        unset($_SESSION['error_login_blank']);
                    }
                ?>
                <input name="haslo" type="password" placeholder="Podaj hasło" />
                
                <?PHP
                    if(isset($_SESSION['error_login_and_password']))
                    {
                        echo '<div class="error">'.$_SESSION['error_login_and_password'].'</div>';
                        unset($_SESSION['error_login_and_password']);
                    }
                ?>
                <input style="width: 30%; background-color: #17b55a; color: #d0f0de;" type="submit" value="Zaloguj się" /> lub <a href="registerPage.php"><input type="button" style="width: 30%; background-color: #17b55a; color: #d0f0de;" value="Zarejestruj się" />
            </form>
        </div>
    </div>
    <p class="demo-icon icon-down-dir-1"></p>
    <div class="footerHelloPage">
        <div class="containerFooterHelloPage">
            artCollection &copy;
            <hr>
            <a href="#">cos tam</a><a href="#">cos tam</a><a href="#">cos tam</a><a href="#">cos tam</a>
        </div>
    </div>
</body>
</html>